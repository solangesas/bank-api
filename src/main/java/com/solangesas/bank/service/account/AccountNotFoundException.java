package com.solangesas.bank.service.account;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The class {@code AccountNotFoundException} represents a bad HTTP request and returns 404.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Not Found")
public class AccountNotFoundException extends RuntimeException {

}
