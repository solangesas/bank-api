package com.solangesas.bank.service.account;

import com.solangesas.bank.domain.account.AccountRepository;
import com.solangesas.bank.domain.client.Client;
import com.solangesas.bank.rest.account.AccountRequest;
import com.solangesas.bank.service.client.ClientService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.solangesas.bank.domain.account.Account;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * The class {@code AccountService} defines services of {@link Account} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@AllArgsConstructor
@Slf4j
@Service
public class AccountService {

    private final AccountRepository repository;
    private final ClientService clientService;

    public List<Account> getAll() {
        return repository.findAll();
    }

    public Account getById(final Integer id) throws AccountNotFoundException {
        final Optional<Account> account = repository.findById(id);
        return account.orElseThrow(AccountNotFoundException::new);
    }

    @Transactional
    public Account save(final AccountRequest request) {
        final Client client = clientService.getById(request.getClient_id());
        return repository.saveAndFlush(new Account(client));
    }

    @Transactional
    public Account update(final Integer id, final Account updatedAccount) {
        final Optional<Account> optAccount = repository.findById(id);

        if (!optAccount.isPresent()) {
            throw new AccountNotFoundException();
        }

        final Account account = optAccount.get();
        account.update(updatedAccount);
        return repository.save(account);
    }

    @Transactional
    public void deleteById(final Integer id) throws AccountNotFoundException {
        final Optional<Account> optAccount = repository.findById(id);

        if (!optAccount.isPresent()) {
            throw new AccountNotFoundException();
        }
        repository.delete(optAccount.get());
    }
}
