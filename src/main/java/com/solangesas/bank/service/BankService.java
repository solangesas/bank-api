package com.solangesas.bank.service;

import com.solangesas.bank.domain.transaction.Transaction;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * The class {@code BankService} defines services to handle {@link Transaction}.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@AllArgsConstructor
@Slf4j
@Service
public class BankService {

}
