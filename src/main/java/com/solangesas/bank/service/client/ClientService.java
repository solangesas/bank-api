package com.solangesas.bank.service.client;

import com.solangesas.bank.domain.client.Client;
import com.solangesas.bank.domain.client.ClientRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * The class {@code ClientService} defines services of {@link Client} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@AllArgsConstructor
@Slf4j
@Service
public class ClientService {

    private final ClientRepository repository;

    public List<Client> getAll() {
        return repository.findAll();
    }

    public Client getById(final Integer id) {
        final Optional<Client> client = repository.findById(id);
        return client.orElseThrow(ClientNotFoundException::new);
    }

    @Transactional
    public Client save(final Client client) {
        return repository.saveAndFlush(client);
    }

    @Transactional
    public Client update(final Integer id, final Client updatedClient) {
        final Optional<Client> optClient = repository.findById(id);

        if (!optClient.isPresent()) {
            throw new ClientNotFoundException();
        }

        final Client client = optClient.get();
        client.update(updatedClient);
        return repository.save(client);
    }

    @Transactional
    public void deleteById(final Integer id) {
        final Optional<Client> optClient = repository.findById(id);

        if (!optClient.isPresent()) {
            throw new ClientNotFoundException();
        }
        repository.delete(optClient.get());
    }

}
