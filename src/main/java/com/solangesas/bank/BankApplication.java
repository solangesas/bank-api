package com.solangesas.bank;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The class {@code BankApplication} starts the Backend server.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/16/18
 */
@Slf4j
@SpringBootApplication
public class BankApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		log.info("#####Backend server started#####");
	}

}

