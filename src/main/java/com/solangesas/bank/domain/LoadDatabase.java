package com.solangesas.bank.domain;

import com.solangesas.bank.domain.account.Account;
import com.solangesas.bank.domain.account.AccountRepository;
import com.solangesas.bank.domain.client.Client;
import com.solangesas.bank.domain.client.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The type Load database.
 */
@Configuration
@Slf4j
public class LoadDatabase {

    /**
     * Init database command line runner.
     *
     * @param clientRepository {@link ClientRepository}
     * @param accountRepository {@link AccountRepository}
     * @return the command line runner
     */
    @Bean
    CommandLineRunner initDatabase(ClientRepository clientRepository, AccountRepository accountRepository) {
        return null;

  /*      Client client = new Client("Sol Soares", "teste@teste.com", "password");
        return args -> {
            log.info("Preloading " + clientRepository.save(client));
            log.info("Preloading " + accountRepository.save(new Account(client)));
        };*/
    }
}
