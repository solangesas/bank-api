package com.solangesas.bank.domain.account;

import com.solangesas.bank.rest.account.AccountRequest;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * The class {@code AccountSpecification} defines {@link Account} Specification.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@AllArgsConstructor
public class AccountSpecification implements Specification<Account> {

    private final AccountRequest request;

    @Override
    public Predicate toPredicate(Root<Account> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        if (!StringUtils.isEmpty(request)) {
            if (!StringUtils.isEmpty(request.getClient_id()))
                return criteriaBuilder.equal(root.get("client_id"), request.getClient_id());
        }
        return null;
    }

}
