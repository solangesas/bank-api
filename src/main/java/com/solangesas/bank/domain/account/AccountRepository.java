package com.solangesas.bank.domain.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * The interface {@code AccountRepository} is the repository of {@link Account} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {
}
