package com.solangesas.bank.domain.account;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.solangesas.bank.domain.client.Client;
import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * The entity {@code Account} represents an account
 * @author Solange Soares
 * @version 1.0
 * @since 02/16/19
 */
@EqualsAndHashCode(of = "account_id")
@Data
@AllArgsConstructor
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer account_id;

    @ManyToOne
    @JoinColumn(name="accounts")
    @JsonBackReference
    private Client client;

    private BigDecimal balance = BigDecimal.ZERO;

    private static final int SCALE = 2;

    public Account(){}

    public Account(final Client client) {
        this.client = client;
    }

    /**
     * Add the value received into balance
     * @param value The amount that will be deposit into the account
     * @return {@code BigDecimal} The balance amount after the deposit
     */
    public BigDecimal deposit(BigDecimal value) {
        balance = balance.add(value).setScale(SCALE, BigDecimal.ROUND_HALF_UP);
        return balance;
    }

    /**
     *
     * @param value The amount that will be withdraw from the account
     * @return {@code BigDecimal} The balance amount after the withdraw
     */
    public BigDecimal withdraw(BigDecimal value) {
        balance = balance.subtract(value).setScale(SCALE, BigDecimal.ROUND_HALF_UP);
        return balance;
    }

    public void update(final Account account) {
        if(!StringUtils.isEmpty(account.getClient())) {
            this.client = account.getClient();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Account: ");
        sb.append("client: ").append(client.getName());
        sb.append(", balance: ").append(balance);
        return sb.toString();
    }

}
