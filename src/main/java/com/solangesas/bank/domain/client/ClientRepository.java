package com.solangesas.bank.domain.client;

import com.solangesas.bank.domain.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * The interface {@code ClientRepository} is the repository of {@link Client} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
public interface ClientRepository extends JpaRepository<Client, Integer>, JpaSpecificationExecutor<Client> {
}
