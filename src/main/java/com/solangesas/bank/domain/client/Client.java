package com.solangesas.bank.domain.client;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.solangesas.bank.domain.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

/**
 * The entity {@code Client} represents a client
 * @author Solange Soares
 * @version 1.0
 * @since 02/16/19
 */
@EqualsAndHashCode(of = "client_id")
@Data
@AllArgsConstructor
@Entity
public class Client {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer client_id;

    private String name;

    @Email
    @Column(unique = true)
    private String email;

    private String password;

    @OneToMany(mappedBy="client")
    @JsonManagedReference
    private List<Account> accounts;

    public Client() {}

    public Client(final String name, final String email, final String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public void update(final Client client) {

        if(!StringUtils.isEmpty(client.getName())) {
            this.name = client.getName();
        }

        if(!StringUtils.isEmpty(client.getEmail())) {
            this.email = client.getEmail();
        }

        if(!StringUtils.isEmpty(client.getPassword())) {
            this.password = client.getPassword();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Client: ");
        sb.append("client_id: ").append(client_id);
        sb.append(", name: ").append(name);
        sb.append(", email: ").append(email);
        return sb.toString();
    }
}
