package com.solangesas.bank.domain.transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * The interface {@code TransactionRepository} is the repository of {@link Transaction} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
public interface TransactionRepository extends JpaRepository<Transaction, Integer>, JpaSpecificationExecutor<Transaction> {
}
