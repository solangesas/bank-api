package com.solangesas.bank.domain.transaction;

import com.solangesas.bank.domain.account.Account;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * The entity {@code Transaction} represents a transaction
 * @author Solange Soares
 * @version 1.0
 * @since 02/16/19
 */
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Account account;
    private BigDecimal value;
    private TransactionType type;
    private Instant date;

    public Transaction(final Account account, final BigDecimal value, final TransactionType type,
                       final Instant date) {
        this.account = account;
        this.value = value;
        this.type = type;
        this.date = date;
    }
}
