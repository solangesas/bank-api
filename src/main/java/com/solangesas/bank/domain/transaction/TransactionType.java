package com.solangesas.bank.domain.transaction;

/**
 * Enum with possible types of transaction
 * @author Solange Soares
 */
public enum TransactionType {
    DEBIT,CREDIT;
}
