package com.solangesas.bank.rest.account;

import lombok.Value;
import com.solangesas.bank.domain.account.Account;

/**
 * The class {@code AccountRequest} represents {@link Account} request parameters for search criteria.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@Value
public class AccountRequest {

    Integer client_id;

    public AccountRequest() {
        client_id = null;
    }

    public AccountRequest(final Integer client_id) {
        this.client_id = client_id;
    }
}
