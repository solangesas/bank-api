package com.solangesas.bank.rest.account;

import com.solangesas.bank.domain.account.Account;
import com.solangesas.bank.domain.account.AccountSpecification;
import com.solangesas.bank.service.account.AccountService;
import com.solangesas.bank.service.client.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The class {@code AccountController} defines the REST API for {@link Account} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@AllArgsConstructor
@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService service;

    @GetMapping
    public List<Account> getAccounts() {
        return service.getAll();
    }

    @PostMapping
    public Account addAccount(@Valid @ModelAttribute final AccountRequest request){
        return service.save(request);
    }

    @GetMapping("/{id}")
    public Account getAccountById(@PathVariable final Integer id) {
        return service.getById(id);
    }

    @PutMapping("/{id}")
    public Account updateAccount(@PathVariable final Integer id,
                               @Valid @RequestBody final Account updatedAccount) {
        return service.update(id, updatedAccount);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable final Integer id) {
        service.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
