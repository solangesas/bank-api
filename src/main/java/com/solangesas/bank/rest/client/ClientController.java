package com.solangesas.bank.rest.client;

import com.solangesas.bank.domain.client.Client;
import com.solangesas.bank.service.client.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


import java.util.List;

/**
 * The class {@code ClientController} defines the REST API for {@link Client} resource.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@AllArgsConstructor
@RestController
@RequestMapping("/clients")
public class ClientController {

    private final ClientService service;

    @GetMapping
    public List<Client> getClients() {
        return service.getAll();
    }

    @PostMapping
    public Client addClient(@Valid @RequestBody final Client newClient) {
        return service.save(newClient);
    }

    @GetMapping("/{id}")
    public Client getClientById(@PathVariable final Integer id) {
        return service.getById(id);
    }

    @PutMapping("/{id}")
    public Client updateClient(@PathVariable final Integer id,
                                               @Valid @RequestBody final Client updatedClient) {
        return service.update(id, updatedClient);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable final Integer id) {
        service.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}