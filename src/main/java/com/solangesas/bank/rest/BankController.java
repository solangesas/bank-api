package com.solangesas.bank.rest;

import com.solangesas.bank.domain.account.Account;
import com.solangesas.bank.domain.transaction.Transaction;
import com.solangesas.bank.service.BankService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BankController {

    private BankService service;

    public List<Transaction> getTransactions() {
        return null;
    }

    @GetMapping("/{account_id}")
    public Account getBalance(@PathVariable final Integer id) {
        return null;
    }

    //withdraw
    //deposit
    //getBalance
    //getStatement
}
