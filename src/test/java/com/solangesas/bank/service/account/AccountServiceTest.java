package com.solangesas.bank.service.account;

import com.solangesas.bank.domain.account.Account;
import com.solangesas.bank.domain.client.Client;
import com.solangesas.bank.rest.account.AccountRequest;
import com.solangesas.bank.service.client.ClientService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * The class {@code AccountServiceTest} tests the methods of {@link AccountService}.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    private AccountService service;

    @Autowired
    private ClientService clientService;

    @Test
    public void getAll() {
        Client client = new Client("Name1", "test1@email.com", "password");
        clientService.save(client);
        AccountRequest request = new AccountRequest(client.getClient_id());
        service.save(request);
        List<Account> accounts = service.getAll();

        Assert.assertEquals(1, accounts.size());
    }

    @Test
    public void getById() {
        Client client = new Client("Name2", "test2@email.com", "password");
        clientService.save(client);
        AccountRequest request = new AccountRequest(client.getClient_id());
        Account account = service.save(request);
        Account accountGotById = service.getById(account.getAccount_id());

        Assert.assertNotNull(accountGotById);
    }

    @Test(expected = AccountNotFoundException.class)
    public void getByIdAccountNotFound() {
        service.getById(10);
    }

    @Test
    public void save() {
        Client client = new Client("Name3", "test3@email.com", "password");
        clientService.save(client);
        AccountRequest request = new AccountRequest(client.getClient_id());
        Account account = service.save(request);

        Assert.assertNotNull(account);
    }

    @Test
    public void update() {
        Client client = new Client("Name1", "test4@email.com", "password");
        clientService.save(client);
        AccountRequest request = new AccountRequest(client.getClient_id());
        Account account = service.save(request);

        Client client2 = new Client("Name2", "test4_1@email.com", "password");
        clientService.save(client2);

        account.setClient(client2);
        service.update(client.getClient_id(), account);

        Assert.assertEquals("Name2", account.getClient().getName());

    }

    @Test(expected = AccountNotFoundException.class)
    public void deleteById() {
        Client client = new Client("Name", "test5@email.com", "password");
        clientService.save(client);
        AccountRequest request = new AccountRequest(client.getClient_id());
        Account account = service.save(request);

        service.deleteById(account.getAccount_id());

        service.getById(account.getAccount_id());
    }
}