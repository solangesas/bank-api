package com.solangesas.bank.service.client;

import com.solangesas.bank.domain.client.Client;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * The class {@code ClientServiceTest} tests the methods of {@link ClientService}.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTest {

    @Autowired
    private ClientService service;

    @Test
    public void getAll() {
        service.save(new Client("Name1", "test1@email.com", "password"));
        service.save(new Client("Name2", "test2@email.com", "password"));
        service.save(new Client("Name3", "test3@email.com", "password"));

        List<Client> clients = service.getAll();
        Assert.assertEquals(3, clients.size());
    }

    @Test
    public void getById() {
        Assert.assertNotNull(service.getById(1));
    }

    @Test(expected = ClientNotFoundException.class)
    public void getByIdClientNotFound() {
        service.getById(10);
    }

    @Test
    public void save() {
        Assert.assertNotNull(service.save(new Client("Name4", "test4@email.com", "password")));
    }

    @Test
    public void update() {
        Client client = service.save(new Client("Name5", "test5@email.com", "password"));
        client.setName("Updated Name");
        Client updatedClient = service.update(client.getClient_id(), client);

        Assert.assertEquals("Updated Name", updatedClient.getName());
    }

    @Test(expected = ClientNotFoundException.class)
    public void updateClientNotFound() {
        service.update(10, new Client());
    }

    @Test(expected = ClientNotFoundException.class)
    public void deleteById() {
        Client client = service.save(new Client("Name6", "test6@email.com", "password"));
        service.deleteById(client.getClient_id());
        service.getById(client.getClient_id());
    }
}