package com.solangesas.bank.domain.account;

import com.solangesas.bank.domain.client.Client;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * The class {@code AccountTest} tests the methods of {@link Account}.
 *
 * @author Solange Soares
 * @version 1.0
 * @since 02/17/19
 */
public class AccountTest {

    private static final BigDecimal EIGHTY = new BigDecimal("80.00");
    private static final BigDecimal SIXTY = new BigDecimal("60.00");
    private static final BigDecimal TWENTY = new BigDecimal("20.00");
    private static final BigDecimal NEGATIVE_TWENTY = new BigDecimal("-20.00");

    @Test
    public void testDepositWithZeroBalance() {
        Account account = new Account(new Client());
        BigDecimal balance = account.deposit(TWENTY);
        Assert.assertEquals(TWENTY, balance);
    }

    @Test
    public void testMultipleDeposits() {
        Account account = new Account(new Client());
        account.deposit(TWENTY);
        account.deposit(TWENTY);
        account.deposit(TWENTY);
        BigDecimal balance = account.deposit(TWENTY);
        Assert.assertEquals(EIGHTY, balance);
    }

    @Test
    public void testWithdrawWithZeroBalance() {
        Account account = new Account(new Client());
        BigDecimal balance = account.withdraw(TWENTY);
        Assert.assertEquals(NEGATIVE_TWENTY, balance);
    }

    @Test
    public void testWithdrawWithPositiveBalance() {
        Account account = new Account(new Client());
        account.deposit(EIGHTY);
        BigDecimal balance = account.withdraw(TWENTY);
        Assert.assertEquals(SIXTY, balance);
    }
}